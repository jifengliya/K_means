package com.KMeans;

public interface Item{

	/**
	 * 将数据存入对象中 存入成功返回true， 存入失败返回false
	 * @param dimension 数据项维数 一维数据{1} 二维数据{2}
	 * @return
	 */
	public boolean setData(int dimension, double data);
	
	/**
	 * 根据输入的维数取出数据
	 * @param dimension
	 * @return
	 */
	public double getData(int dimension);
	
}
