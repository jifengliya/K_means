package com.KMeans.test;

import java.util.ArrayList;
import java.util.List;
import com.KMeans.Cluster;
import com.KMeans.Item;
import com.KMeans.KMeans;
import com.KMeans.Utils;
import java.util.Comparator;

public class Test {
	public static void main(String[] args){
		List<TestItem> datas = new ArrayList<TestItem>();
		
		TestItem item1 = new TestItem(1.111111, 12, 100);
		TestItem item2 = new TestItem(4.0, 12, 300);
		TestItem item3 = new TestItem(8.0, 9, 500);
		
		datas.add(item1);
		datas.add(new TestItem(1.0, 10, 98));
		datas.add(new TestItem(2, 4, 97));
		datas.add(item2);
		datas.add(new TestItem(1.0, 11, 110));
		datas.add(new TestItem(3.0, 13, 120));
		datas.add(new TestItem(3.3, 12.6, 102));
		datas.add(item3);
		datas.add(new TestItem(7.4, 10, 330));
		datas.add(new TestItem(8.4, 11, 320));
		datas.add(new TestItem(3, 7, 314));
		datas.add(new TestItem(3, 9, 340));
		datas.add(new TestItem(8, 6, 338));
		datas.add(new TestItem(9, 7, 499));
		datas.add(new TestItem(3, 10, 500));
		datas.add(new TestItem(2, 6, 530));
		datas.add(new TestItem(9, 6, 550));
		datas.add(new TestItem(2, 9, 540));
		
		int dimension = 2;
		int[] dimensionKeys = new int[2];
		dimensionKeys[0] = 0;
		dimensionKeys[1] = 2;
		
		class Comparator implements java.util.Comparator<Item>{

			public int compare(Item obj1, Item obj2) {
				int result = 0;
				TestItem o1 = (TestItem)obj1;
				TestItem o2 = (TestItem)obj2;
				if(o1.getX() > o2.getX())
					result = 1;
				else if(o1.getX() < o2.getX())
					result = -1;
				return result;
			}
		}
		
		int[] centerIndexs = new int[]{0, 4, 6};
		
//		int[] centerIndexs = new int[]{0, 1, 2};
		
		KMeans<TestItem> kMeans = new KMeans<TestItem>(dimension, 3, datas, new Comparator());
		kMeans.setDimensionKeys(dimensionKeys);
		int count = kMeans.beginAdjust(5);
		Cluster<TestItem>[] clusters = kMeans.getClusters();
		
		Utils.printCluster(clusters);
		System.out.println(count);
		
/*	
		kMeans.doAdjust();
		printCluster(clusters, dimension);
		
		kMeans.doAdjust();
		printCluster(clusters, dimension);
		
		kMeans.doAdjust();
		printCluster(clusters, dimension);
		
		kMeans.doAdjust();
		printCluster(clusters, dimension);
		*/
	
	}
	
	
}
