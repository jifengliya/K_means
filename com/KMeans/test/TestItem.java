package com.KMeans.test;

import com.KMeans.Item;

public class TestItem implements Item{

	private double x;
	
	private double y;
	
	private double z;
	
	public TestItem() {}
	
	public TestItem(double x) {
		this.x = x;
	}
	
	public TestItem(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public TestItem(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	@Override
	public boolean setData(int dimension, double data) {
		boolean result = true;
		switch(dimension){
			case 0 : x = data;break;
			case 1 : y = data;break;
			case 2 : z = data;break;
			default : x = data;break;
		}
		return result;
	}

	@Override
	public double getData(int dimension) {
		double data = 0;
		switch(dimension){
			case 0 : data = x;break;
			case 1 : data = y;break;
			case 2 : data = z;break;
			default : data = x;break;
		}
		return data;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}
	
}
