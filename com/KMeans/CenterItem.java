package com.KMeans;

import java.util.HashMap;
import java.util.Map;

/**
 * 质心
 * @author Administrator
 *
 */
public class CenterItem implements Item{

	private Map<Integer, Double> dataMap;
	
	public CenterItem() {
		this.dataMap = new HashMap<Integer, Double>();
	}
	
	@Override
	public boolean setData(int dimension, double data) {
		dataMap.put(dimension, data);
		return true;
	}

	@Override
	public double getData(int dimension) {
		return dataMap.get(dimension);
	}
	
}
