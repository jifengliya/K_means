package com.KMeans;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class KMeans<T extends Item> {
	
	/**
	 * 数据维度数
	 */
	private int dimension;
	
	private int[] dimensionKeys;
	
	private List<T> datas;
	
	private Cluster<T>[] clusters;
	
	private int centers;
	
	/**
	 * 上次聚类后产生的质心
	 */
	private CenterItem[] prevCenterItems;

	/**
	 * 初始化时产生的质心
	 */
	private Item[] initCenterItems;
	
	/**
	 * 节点比较器用于各个节点比较
	 */
	private Comparator<Item> comparator;
	
	/**
	 * @param dimension 数据维数
	 * @param centers   聚合成几个分类[有几个质心]没有提供质心的情况下，质心将随机生成
	 * @param datas     数据项集合
	 */
	public KMeans(int dimension, int centers, List<T> datas) {
		this(dimension, centers, datas, null);
	}
	
	/**
	 * @param dimension  数据维数
	 * @param centers    聚合成几个分类[有几个质心]没有提供质心的情况下，质心将随机生成
	 * @param datas      数据项集合
	 * @param comparator 数据比较器，用于排序
	 */
	public KMeans(int dimension, int centers, List<T> datas, Comparator<Item> comparator) {
		this(dimension, datas, comparator, Utils.getRandom(centers, datas.size()));
	}
	
	/**
	 * @param dimension 数据维数
	 * @param datas     数据项集合
	 * @param comparator 数据比较器，用于排序
	 * @param centerIndexs 作为质心节点的索引号
	 */
	public KMeans(int dimension, List<T> datas, Comparator<Item> comparator, int[] centerIndexs){
		Item[] centerItems = new Item[centerIndexs.length];
		for(int i = 0; i < centerIndexs.length; i++){
			centerItems[i] = datas.get(centerIndexs[i]);
		}
		this.init(dimension, datas, comparator, centerItems);
	}
	
	/**
	 * @param dimension 数据维数
	 * @param datas     数据项集合
	 * @param comparator 数据比较器，用于排序
	 * @param centerItems 作为质心的节点
	 */
	public KMeans(int dimension, List<T> datas, Comparator<Item> comparator, Item[] centerItems){
		this.init(dimension, datas, comparator, centerItems);
	}
	
	private void init(int dimension, List<T> datas, Comparator<Item> comparator, Item[] centerItems) {
		this.dimension       = dimension;
		this.datas           = datas;
		this.centers         = centerItems.length;
		this.clusters        = new Cluster[centerItems.length];
		this.comparator      = comparator;
		this.initCenterItems = centerItems;
		this.prevCenterItems = new CenterItem[centerItems.length];
		this.dimensionKeys   = new int[this.dimension];
		
		this.dimensionKeys = Utils.createDimensionKeys(this.dimension);
		initCenterItems(centerItems, dimensionKeys, this.comparator);
	}
	
	private void initCenterItems(Item[] centerItems, int[] dimensionKeys, Comparator<Item> comparator){
		for(int i = 0; i < centerItems.length; i++){
			clusters[i] = new Cluster<T>(dimensionKeys, createCenterItem(centerItems[i], dimensionKeys), comparator);
		}
	}
	
	/**
	 * 设置维度映射
	 * @param dimensionKeys
	 */
	public void setDimensionKeys(int[] dimensionKeys) {
		this.dimensionKeys = dimensionKeys;
		this.dimension = this.dimensionKeys.length;
		initCenterItems(this.initCenterItems, this.dimensionKeys, this.comparator);
	}
	
	/**
	 * 默认情况下进行50次操作
	 * @return 返回聚合的次数
	 */
	public int beginAdjust(){
		return beginAdjust(50);
	}
	
	/**
	 * 进行聚类操作,直到质心不在变化为止,或者到达指定次数为止
	 * @param counts 指定聚合次数。如果counts为0,则一直聚合到质心不再变化为止
	 * @return 返回聚合的次数
	 */
	public int beginAdjust(int counts){
		int i = 0;
		if(counts == 0){
			do{
				doAdjust();
				i++;
			}while(!checkCenterItem(this.prevCenterItems, clusters, this.dimensionKeys));
		}else if(counts > 0){
			do{
				doAdjust();
				i++;
			}while(!checkCenterItem(this.prevCenterItems, clusters, this.dimensionKeys) && i < counts);
		}
		return i;
	}
	
	/**
	 * 进行一次聚类操作
	 */
	public void doAdjust(){
		
		for(int i = 0; i < this.clusters.length; i++){
			Cluster<T> cluster = this.clusters[i];
			this.prevCenterItems[i] = cluster.getCenterItem();//记录上次聚类后的质心
			cluster.clear();//清理各集合的数据
		}
		
		for(T item : this.datas){
			double     minDistance = Double.MAX_VALUE;
			Cluster<T> mincluster  = null;
			for(Cluster<T> cluster : this.clusters){
				CenterItem centerItem = cluster.getCenterItem();
				double distance = Utils.getDistance(centerItem, item, this.dimensionKeys);
				if(distance < minDistance){
					minDistance = distance;
					mincluster  = cluster;
				}
			}
			mincluster.addItem(item, minDistance);
		}
		
		for(Cluster<T> cluster : this.clusters){
			cluster.setCenterItem();//重新计算各数据集合的质心
		}
		
	}
	
	private CenterItem createCenterItem(Item item, int[] dimensionKeys){
		CenterItem centerItem = new CenterItem();
		Utils.copyItemData(item, centerItem, dimensionKeys);
		return centerItem;
	}
	
	/**
	 * 检查质心是否有变化
	 * @param centerItems
	 * @param clusters
	 * @param dimension
	 * @return
	 */
	private boolean checkCenterItem(CenterItem[] centerItems, Cluster<T>[] clusters, int[] dimensionKeys){
		boolean result = true;
		for(int i = 0; i < clusters.length; i++){
			result = Utils.itemEqualsTo(centerItems[i], clusters[i].getCenterItem(), dimensionKeys);
			if(!result)
				break;
		}
		return result;
	}
	
	public Cluster<T>[] getClusters() {
		return clusters;
	}
	
	public Cluster<T>[] getClusters(Comparator<Cluster> comparator) {
		Arrays.sort(clusters, comparator);
		return clusters;
	}

	public int getCenters() {
		return centers;
	}

	public int[] getDimensionKeys() {
		return dimensionKeys;
	}
	
	public Comparator<Item> getComparator() {
		return comparator;
	}

	public void setComparator(Comparator<Item> comparator) {
		this.comparator = comparator;
	}

	public Item[] getInitCenterItems() {
		return initCenterItems;
	}

}
